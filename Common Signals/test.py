import cv2
import numpy as np
import sys
import time
import sys
vid = sys.argv[1]
cap = cv2.VideoCapture(vid)

# Exit if video not opened.
if not cap.isOpened():
    print ("Could not open video")
    sys.exit()

# Read first frame.
lower_orange = np.array([5, 140, 140])
upper_orange = np.array([10,250,250])

lower_red = np.array([0, 140, 140])
upper_red = np.array([10,250,250])

while(1):
	cv2.waitKey(0)
	_, frame = cap.read()
	blur = cv2.GaussianBlur(frame, (11,11), 0)
	hsv = cv2.cvtColor(blur, cv2.COLOR_BGR2HSV)

    
   # Threshold the HSV image to get only blue colors
	mask = cv2.inRange(hsv, lower_red, upper_red)
	# mask = cv2.erode(mask, None, iterations=2)
	# mask = cv2.dilate(mask, None, iterations=2)
    # Bitwise-AND mask and original image
	# res = cv2.bitwise_and(frame,frame, mask= mask)
	cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
	cnts = cnts[1]

	cv2.drawContours(frame, cnts, -1, (0, 255, 200), 3)
	cv2.imshow('frame',frame)
	cv2.imshow('mask',mask)
	# cv2.imshow('res',res)
	k = cv2.waitKey(1) & 0xFF
	if k == 27:
		break

	# cv2.waitKey(0)
	# break
cv2.destroyAllWindows()